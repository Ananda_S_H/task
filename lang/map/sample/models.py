from __future__ import unicode_literals

from django.db import models

# Create your models here.
import openpyxl
from geopy.geocoders import Nominatim


def fetch():
	data = openpyxl.load_workbook('/home/gtpl/Desktop/map/lang/map/sample.xlsx')
	sheet = data.get_sheet_by_name('Sheet1')
	for cell in range(1,sheet.max_row+1):
		val=(sheet.cell(row=cell,column=1).value)
		geolocator = Nominatim()
		location = geolocator.geocode(val)
		lat = location.latitude
		longi = location.longitude
		sheet.cell(row=cell,column=2).value=lat
		sheet.cell(row=cell,column=3).value=longi
		data.save('/home/gtpl/Desktop/map/lang/map/sample.xlsx')
fetch()